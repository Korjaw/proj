### username/password ###
admin/admin
user/user

### database default settings ##
database: accounts2
login: root
password: root

### SQL ###

CREATE TABLE `access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `access`
(`id`,`name`)
VALUES
(1,"role user");


INSERT INTO `access`
(`id`,`name`)
VALUES
(2,"role admin");


CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teamName` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `team`
(`id`,
`teamName`,
`username`)
VALUES
(1,"admin-team",
1);

INSERT INTO `team`
(`id`,
`teamName`,
`username`)
VALUES
(2,"admin-team2",
1);

INSERT INTO `team`
(`id`,
`teamName`,
`username`)
VALUES
(3,"user-team",
2);

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

INSERT INTO `user`
(`id`,`username`,`password`,`name`,`surname`)
VALUES("1","admin","$2a$11$EwlqTLW7.8HQs5FTp3Nia.u36i0gtyOmDuuy8ZqY07KrRK0UsbGDS","admin_name","admin_surname");

INSERT INTO `user`
(`id`,`username`,`password`,`name`,`surname`)
VALUES("2","user","$2a$11$Wax/9JFJGWQsqIGXFKX8KOQtdffZJm37ihoZqXdhY61JTCn4kIFBa","user_name","user_surname");


CREATE TABLE `user_access` (
  `user_id` int(11) NOT NULL,
  `access_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`access_id`),
  KEY `fk_user_role_roleid_idx` (`access_id`),
  CONSTRAINT `fk_user_role_userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user_access`
(`user_id`,
`access_id`)
VALUES
(1,2);
INSERT INTO `user_access`
(`user_id`,
`access_id`)
VALUES
(2,1);