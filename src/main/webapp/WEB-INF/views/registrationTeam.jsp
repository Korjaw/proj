<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page extends="pl.sda.korjaw.database.Config" %>
<%@ page import="java.io.*,java.sql.*" %>

<!DOCTYPE html>
<head>
    <title>Stworz team</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<style>
    body {
        padding-top: 40px;
        padding-bottom: 40px;
        padding-left: 40px;
        background-color: #eee;
    }

    .registerteam {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }


    .registerteam .form-control {
        position: relative;
        height: auto;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        padding: 10px;
        font-size: 16px;
        font-family: Tahoma;
    }

    .registerteam .form-control:focus {
        z-index: 2;
    }

    .registerteam input[type="username"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
    }

    .registerteam input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    .registerteam input[type="passwordConfirm"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    .registerteam input[type="name"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    .registerteam input[type="surname"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    ::-webkit-input-placeholder { /* Chrome */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

    :-ms-input-placeholder { /* IE 10+ */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

    ::-moz-placeholder { /* Firefox 19+ */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

    :-moz-placeholder { /* Firefox 4 - 18 */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

</style>
<body>

<%
    String login = request.getUserPrincipal().getName().toString();
    String query = "SELECT * FROM user WHERE username = ?";
    PreparedStatement statement = con.prepareStatement(query);
    statement.setString(1, login);
    ResultSet rs = statement.executeQuery();
    String id = null;
    while (rs.next()) {
        id = rs.getString(1);
    }
    %>

<div class="container">

    <form:form class="registerteam" method="POST" modelAttribute="teamForm">
        <h3>Stworz konto</h3>
        <spring:bind path="teamName">
            <label for="inputTeamName" class="sr-only">Teamname</label>
            <form:input type="text" id="inputTeamName" class="form-control" placeholder="TeamName"
                        path="teamName" autofocus="true" ></form:input>
            <form:errors path="teamName"></form:errors>
        </spring:bind>
        <spring:bind path="username">
            <form:input type="hidden" id="inputTeamName" class="form-control"
                        path="username" value="<%= id %>"></form:input>
            <form:errors path="teamName"></form:errors>
        </spring:bind>
        <button class="btn btn-lg btn-warning btn-block" type="submit">Submit</button>
        <button class="btn btn-lg btn-info btn-block" type="button"
                onclick="window.location.href='http://localhost:8080/welcome'">Powrot
        </button>
    </form:form>
</div>
</body>
</html>
