<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page extends="pl.sda.korjaw.database.Config" %>
<%@ page import="java.io.*,java.sql.*" %>

<!DOCTYPE html>
<head>
    <title>Usun usera</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<style>
    body {
        padding-top: 40px;
        padding-bottom: 40px;
        padding-left: 40px;
        background-color: #eee;
    }

    .registerteam {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }


    .registerteam .form-control {
        position: relative;
        height: auto;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        padding: 10px;
        font-size: 16px;
        font-family: Tahoma;
    }

    .registerteam .form-control:focus {
        z-index: 2;
    }

    .registerteam input[type="username"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
    }

    .registerteam input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    .registerteam input[type="passwordConfirm"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    .registerteam input[type="name"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    .registerteam input[type="surname"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    ::-webkit-input-placeholder { /* Chrome */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

    :-ms-input-placeholder { /* IE 10+ */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

    ::-moz-placeholder { /* Firefox 19+ */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

    :-moz-placeholder { /* Firefox 4 - 18 */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

</style>
<body>
<div class="container">


    <form:form class="registerteam" method="POST" modelAttribute="deleteUserForm">
        <h3>Usun team</h3>
        <spring:bind path="id">
            <label for="inputTeamName" class="sr-only">Teamname</label>

            <form:select name="id" path="id" id="inputTeamName" class="form-control">
                <%
                    String queryTeam = "SELECT * FROM user";
                    PreparedStatement statementTeam = con.prepareStatement(queryTeam);
                    ResultSet rsTeam = statementTeam.executeQuery();
                    String idTeam = null;
                    while (rsTeam.next()) {
                        idTeam = rsTeam.getString(1);
                %>
                <option><%=idTeam%></option>
                <%
                    }
                %>
            </form:select>
            <form:errors path="id"></form:errors>
        </spring:bind>
        <button class="btn btn-lg btn-warning btn-block" type="submit">Submit</button>
        <button class="btn btn-lg btn-info btn-block" type="button"
                onclick="window.location.href='http://localhost:8080/welcome'">Powrot
        </button>
    </form:form>
</div>
</body>
</html>