<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page extends="pl.sda.korjaw.database.Config" %>
<%@ page import="java.io.*,java.sql.*" %>


<!DOCTYPE html>
<head>
    <title>EDIT</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<style>
    body {
        padding-top: 40px;
        padding-bottom: 40px;
        padding-left: 40px;
        background-color: #eee;
    }

    .edit {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }


    .edit .form-control {
        position: relative;
        height: auto;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        padding: 10px;
        font-size: 16px;
        font-family: Tahoma;
    }

    .edit .form-control:focus {
        z-index: 2;
    }

    .edit input[type="name"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
    }

    .edit input[type="surname"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    ::-webkit-input-placeholder { /* Chrome */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

    :-ms-input-placeholder { /* IE 10+ */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

    ::-moz-placeholder { /* Firefox 19+ */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

    :-moz-placeholder { /* Firefox 4 - 18 */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

</style>
<body>
<div>

    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <h2>Witaj, Twoj login to: ${pageContext.request.userPrincipal.name}</h2>

        <%
            String login = request.getUserPrincipal().getName().toString();
            String query = "SELECT * FROM user WHERE username = ?";
            PreparedStatement statement = con.prepareStatement(query);
            statement.setString(1, login);
            ResultSet rs = statement.executeQuery();
            rs.next();
            String id = rs.getString(1);
            String username = rs.getString(2);
            String password = rs.getString(3);
            String name = rs.getString(4);
            String surname = rs.getString(5);
        %>
        <form class="edit" method="POST" action="http://localhost:8080/test?${_csrf.parameterName}=${_csrf.token}">
            <input type="hidden" name="id" value="<%= id %>" readonly><br/>
            <h3>Edytuj pola</h3>
            <label for="inputName" class="sr-only">Name</label>
            <input type="text" id="inputName" name="name" class="form-control" placeholder="Name"  value="<%= name %>" required autofocus>
            <label for="inputSurname" class="sr-only">Name</label>
            <input type="text" id="inputSurname" name="surname" class="form-control" placeholder="Surname"  value="<%= surname %>" required autofocus><br/>
            <button type="submit" class="btn btn-lg btn-warning btn-block">Edytuj</button><br/>
            <button class="btn btn-lg btn-info btn-block" type="button"
                    onclick="window.location.href='http://localhost:8080/welcome'">Powrot
            </button>
        </form>

    </c:if>
    <c:if test="${pageContext.request.userPrincipal.name == null}">
        <c:redirect url="http://localhost:8080/edit"></c:redirect>
    </c:if>

</div>
</body>
</html>
