<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<head>
    <title>Zaloguj sie</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<style>
    body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #eee;
    }

    .signin {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }


    .signin .form-control {
        position: relative;
        height: auto;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        padding: 10px;
        font-size: 16px;
        font-family: Tahoma;
    }

    .signin .form-control:focus {
        z-index: 2;
    }

    .signin input[type="username"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
    }

    .signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    ::-webkit-input-placeholder { /* Chrome */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

    :-ms-input-placeholder { /* IE 10+ */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

    ::-moz-placeholder { /* Firefox 19+ */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

    :-moz-placeholder { /* Firefox 4 - 18 */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

</style>

<body>
<div class="container">

    <form class="signin" method="POST" action="${contextPath}/login">
        <h3>Witaj!</h3>
        <label for="inputUsername" class="sr-only">Username</label>
        <input type="text" id="inputUsername" name="username" class="form-control" placeholder="Username" required
               autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Zaloguj sie</button>
        <button class="btn btn-lg btn-warning btn-block" type="submit"
                onclick="window.location.href='http://localhost:8080/registration'">Zarejestruj sie
        </button>
    </form>


</div>


</body>
</html>
