<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<head>
    <title>Stworz konto</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<style>
    body {
        padding-top: 40px;
        padding-bottom: 40px;
        padding-left: 40px;
        background-color: #eee;
    }

    .register {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }


    .register .form-control {
        position: relative;
        height: auto;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        padding: 10px;
        font-size: 16px;
        font-family: Tahoma;
    }

    .register .form-control:focus {
        z-index: 2;
    }

    .register input[type="username"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
    }

    .register input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    .register input[type="name"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    .register input[type="surname"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    ::-webkit-input-placeholder { /* Chrome */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

    :-ms-input-placeholder { /* IE 10+ */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

    ::-moz-placeholder { /* Firefox 19+ */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

    :-moz-placeholder { /* Firefox 4 - 18 */
        background: url(https://openclipart.org/download/247319/abstract-user-flat-3.svg) no-repeat scroll 10px 10px !important;
        background-size: 20px 20px !important;
        text-align: center !important;
    }

</style>
<body>
<div class="container">


    <form:form class="register" method="POST" modelAttribute="userForm">
        <h3>Stworz konto</h3>
        <spring:bind path="username">
            <label for="inputUsername" class="sr-only">Username</label>
            <form:input type="text" id="inputUsername" class="form-control" placeholder="Username"
                        path="username" autofocus="true" ></form:input>
            <form:errors path="username"></form:errors>
        </spring:bind>
        <spring:bind path="password">
            <label for="inputPassword" class="sr-only">Password</label>
            <form:input type="password" path="password" id="inputPassword" class="form-control" placeholder="Password"></form:input>
            <form:errors path="password"></form:errors>
        </spring:bind>
        <spring:bind path="name">
            <label for="inputName" class="sr-only">Name</label>
                <form:input type="text" path="name"  id="inputName" class="form-control" placeholder="Name"></form:input>
                <form:errors path="name"></form:errors>
        </spring:bind>
        <spring:bind path="surname">
            <label for="inputSurname" class="sr-only">Surname</label>
                <form:input type="text" path="surname" id="inputSurname" class="form-control" placeholder="Surname"></form:input>
                <form:errors path="surname"></form:errors>
        </spring:bind>
        <button class="btn btn-lg btn-warning btn-block" type="submit">Submit</button>
        </form:form>
</div>
</body>
</html>
