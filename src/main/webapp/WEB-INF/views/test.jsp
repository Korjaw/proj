<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page extends="pl.sda.korjaw.database.Config" %>
<%@ page import="java.io.*,java.sql.*" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="_csrf_parameter" content="_csrf" />
    <meta name="_csrf_header" content="X-CSRF-TOKEN" />
    <meta name="_csrf" content="e62835df-f1a0-49ea-bce7-bf96f998119c" />
</head>
<body>
<csrf disabled="true"/>
<c:if test="${pageContext.request.userPrincipal.name != null}">

    <form id="logoutForm" method="POST" action="${contextPath}/logout">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>

    <%
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
    %>
    <%
        String query = "UPDATE user SET name='" + name + "', surname='" + surname + "' WHERE id =" + id;
        PreparedStatement statement = con.prepareStatement(query);
        statement.executeUpdate();
    %>
    <c:redirect url="/"></c:redirect>
</c:if>

<c:if test="${pageContext.request.userPrincipal.name == null}">
null
</c:if>
</body>

</html>