<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page extends="pl.sda.korjaw.database.Config" %>
<%@ page import="java.io.*,java.sql.*" %>


<!DOCTYPE html>
<head>
    <title>Welcome</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<style>
    body {
        padding-top: 40px;
        padding-bottom: 40px;
        padding-left: 40px;
        background-color: #eee;
    }

    button {
        font-size: 14px !important;
        max-width: 100px !important;
        padding: 15px !important;
        margin: 0 auto !important;
        max-height: 40px !important;
    }

</style>
<body>
<div>

    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <h2>Witaj, Twoj login to: ${pageContext.request.userPrincipal.name}
        </h2>

        <%
            String login = request.getUserPrincipal().getName().toString();
            String query = "SELECT * FROM user WHERE username = ?";
            PreparedStatement statement = con.prepareStatement(query);
            statement.setString(1, login);
            ResultSet rs = statement.executeQuery();
            String id = null;
            while (rs.next()) {
                id = rs.getString(1);
                String username = rs.getString(2);
                String password = rs.getString(3);
                String name = rs.getString(4);
                String surname = rs.getString(5);
        %>
        <table class="table table-striped">
            <tbody>
            <tr>
                <th scope="row">ID:</th>
                <td><%=id%>
                </td>
            <tr>
                <th scope="row">Username:</th>
                <td><%=username%>
                </td>
            <tr>
                <th scope="row">Password:</th>
                <td><%=password%>
                </td>
            <tr>
                <th scope="row">Name:</th>
                <td><%=name%>
                </td>
            <tr>
                <th scope="row">Surname:</th>
                <td><%=surname%>
                </td>
            </tbody>
        </table>

        <div>
            <span class="button"><button class="btn btn-lg btn-warning btn-block" type="submit"
                                         onclick="window.location.href='http://localhost:8080/edit'">Edytuj
            </button></span><br/></br>
            <span class="button"><button class="btn btn-lg btn-danger btn-block" type="submit"
                                         onclick="document.forms['logoutForm'].submit()">Wyloguj
        </button></span><br/><br/>
        </div>

        <%
            }
        %>

        <div>
            <%
                String user_id = id;

                String query2 = "SELECT access_id FROM user_access WHERE user_id =" + user_id;
                PreparedStatement statement2 = con.prepareStatement(query2);
                ResultSet rs2 = statement2.executeQuery();
                rs2.next();
                String access = rs2.getString(1);
            %>
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th scope="row" style="max-width: 100px;">ACCESS ID</th>
                </tr>
                <tr>
                    <td style="max-width: 100px;"><%=access%>
                    </td>
                </tr>
                <tr>
                    <th scope="row" style="max-width: 100px;">ACCESS NAME</th>
                </tr>
                <tr>
                    <td style="max-width: 100px;"><% if (access.equals("2")) { %> <%= "admin access" %> <% } else { %> <%= "user access"%> <% } %>
                    </td>
                </tr>
                </tbody>
            </table>

            <% if (access.equals("1")) { %>
            </br></br></br>

            <table class="table table-striped">
                <tbody>
                <tr>
                    <th scope="row">TRESC TYLKO DLA UZYTKOWNIKOW</th>
                </tr>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dignissim eget mi non
                        dignissim. Vestibulum non blandit odio, a convallis nunc. Cras ut sem ac nisi scelerisque
                        hendrerit. Donec suscipit vitae ante et pellentesque. Sed pharetra leo lorem, nec egestas odio
                        mattis sit amet. Sed tincidunt sagittis posuere. Cras eu turpis pharetra, tempor purus sit amet,
                        pharetra est.
                    </td>
                </tbody>
            </table>

            <% } else if (access.equals("2")) { %>

            </br></br></br>

            <table class="table table-striped">
                <tbody>
                <tr>
                    <th scope="row">TRESC TYLKO DLA ADMINOW</th>
                </tr>
                <tr>
                    <td>Opcje dostepne tylko dla adminow: <span class="button"><button class="btn btn-lg btn-danger btn-block" type="submit"
                                                                                       onclick="window.location.href='http://localhost:8080/deleteuser'">Usun usera
            </button></span>
                    </td>
                </tr>
            </table>

            <% } %>
        </div>

        <table class="table table-striped">
            <tbody>
            <tr>
                <th style="max-width:200px; font-weight: normal">TWOJE DRUZYNY:</th>
            </tr>
        <%
            String queryTeam = "SELECT * FROM team WHERE username =" + user_id;
            PreparedStatement statementTeam = con.prepareStatement(queryTeam);
            ResultSet rsTeam = statementTeam.executeQuery();
            String idTeam = null;
            while (rsTeam.next()) {
                idTeam = rsTeam.getString(1);
                String teamName = rsTeam.getString(2);
                String ownerID = rsTeam.getString(3);
        %>
            <tr>
                <th style="max-width:200px; font-weight: normal">ID: <b><%=idTeam%></b> | TeamName: <b><%=teamName%></b></th>
            </tr>
        <%
            }
        %>
            </tbody>
        </table>

        <table class="table table-striped">
            <tbody>
            <tr>
                <th style="max-width:200px; font-weight: normal">POZOSTALE DRUZYNY:</th>
            </tr>
            <%
                String queryTeamOther = "SELECT * FROM team WHERE username <>" + user_id;
                PreparedStatement statementTeamOther = con.prepareStatement(queryTeamOther);
                ResultSet rsTeamOther = statementTeamOther.executeQuery();
                String idTeamOther = null;
                while (rsTeamOther.next()) {
                    idTeam = rsTeamOther.getString(1);
                    String teamName = rsTeamOther.getString(2);
                    String ownerID = rsTeamOther.getString(3);
            %>
            <tr>
                <th style="max-width:200px; font-weight: normal">ID: <b><%=idTeam%></b> | TeamName: <b><%=teamName%></b></th>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>


        <div>
            <span class="button"><button class="btn btn-lg btn-success btn-block" type="submit"
                                         onclick="window.location.href='http://localhost:8080/registrationteam'">Stworz team
            </button></span><br/></br>

            <% if(access.equals("2")) {
                %> <span class="button"><button class="btn btn-lg btn-danger btn-block" type="submit"
                                                onclick="window.location.href='http://localhost:8080/deleteteam'">Usun team
            </button></span><br/></br> <%
            }

            %>
        </div>
    </c:if>
    <c:if test="${pageContext.request.userPrincipal.name == null}">
        <c:redirect url="/login"></c:redirect>
    </c:if>

</div>
</body>
</html>
