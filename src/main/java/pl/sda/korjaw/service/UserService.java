package pl.sda.korjaw.service;

import pl.sda.korjaw.model.User;

public interface UserService {
    void save(User user);

    void delete(User user);


    User findByUsername(String username);
}
