package pl.sda.korjaw.service;

import pl.sda.korjaw.model.Team;

public interface TeamService {
    void save(Team team);

    void delete(Team team);


    Team findByTeamName(String teamName);
}
