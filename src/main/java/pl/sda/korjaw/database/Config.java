package pl.sda.korjaw.database;

import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public abstract class Config
        extends HttpServlet
        implements HttpJspPage
{
    protected Connection con;

    public void init(ServletConfig config)
            throws ServletException
    {
        super.init(config);
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost/accounts2","root","root");
        }
        catch (Exception e) {
            throw new UnavailableException(e.getMessage());
        }

        jspInit();
    }
    public void destroy()
    {
        try {
            if (con != null) {
                con.close();
                con = null;
            }
        }
        catch (Exception ignore) {}

        jspDestroy();
        super.destroy();
    }

    /**
     * Called when the JSP is loaded.
     * By default does nothing.
     */
    public void jspInit() {}

    /**
     * Called when the JSP is unloaded.
     * By default does nothing.
     */
    public void jspDestroy() {}

    /**
     * Invokes the JSP's _jspService method.
     */
    public final void service(
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException
    {
        _jspService(request, response);
    }

    /**
     * Handles a service request.
     */
    public abstract void _jspService(
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException;
}