package pl.sda.korjaw.controller;

import pl.sda.korjaw.model.User;
import pl.sda.korjaw.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new User());
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        userService.save(userForm);
        return "redirect:/welcome";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Blad.");
        if (logout != null)
            model.addAttribute("message", "Wylogowales sie.");
        return "login";
    }
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String edit(Model model) {
        return "edit";
    }

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model) {
        return "welcome";
    }

    @RequestMapping(value = {"/test"}, method = RequestMethod.POST)
    public String test() {
        return "test";
    }

    @RequestMapping(value = "/deleteuser", method = RequestMethod.GET)
    public String deleteuser(Model model) {
        model.addAttribute("deleteUserForm", new User());
        return "deleteUser";
    }

    @RequestMapping(value = "/deleteuser", method = RequestMethod.POST)
    public String deleteteam(@ModelAttribute("deleteUserForm") User deleteUserForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "deleteUser";
        }
        userService.delete(deleteUserForm);
        return "redirect:/welcome";
    }

}
