package pl.sda.korjaw.controller;

import pl.sda.korjaw.model.Team;
import pl.sda.korjaw.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class TeamController {
    @Autowired
    private TeamService teamService;

    @RequestMapping(value = "/registrationteam", method = RequestMethod.GET)
    public String registrationteam(Model model) {
        model.addAttribute("teamForm", new Team());
        return "registrationTeam";
    }

    @RequestMapping(value = "/registrationteam", method = RequestMethod.POST)
    public String registrationteam(@ModelAttribute("teamForm") Team teamForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "registrationTeam";
        }
        teamService.save(teamForm);
        return "redirect:/welcome";
    }

    @RequestMapping(value = "/deleteteam", method = RequestMethod.GET)
    public String deleteteam(Model model) {
        model.addAttribute("deleteTeamForm", new Team());
        return "deleteTeam";
    }

    @RequestMapping(value = "/deleteteam", method = RequestMethod.POST)
    public String deleteteam(@ModelAttribute("deleteTeamForm") Team deleteTeamForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "deleteTeam";
        }
        teamService.delete(deleteTeamForm);
        return "redirect:/welcome";
    }

}