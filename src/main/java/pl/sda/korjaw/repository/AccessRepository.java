package pl.sda.korjaw.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.korjaw.model.Access;

public interface AccessRepository extends JpaRepository<Access, Long> {
}






